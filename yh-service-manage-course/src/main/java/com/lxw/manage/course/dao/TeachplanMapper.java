package com.lxw.manage.course.dao;

import com.lxw.framework.domain.course.ext.TeachplanNode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * 课程计划mapper
 * Created by Administrator.
 */
@Component
@Mapper
public interface TeachplanMapper {
    //课程计划查询
    public TeachplanNode selectList(@Param("courseId") String courseId);


    /**
     * 根据id删除课程
     * @param id
     */
    @Select("delete from teachplan where parentid=#{parentid}")
    void deleByPaent(@Param("parentid") String id);
}
