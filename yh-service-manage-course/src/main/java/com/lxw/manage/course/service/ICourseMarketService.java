package com.lxw.manage.course.service;


import com.lxw.framework.domain.course.CourseMarket;
import com.lxw.framework.model.response.ResponseResult;

public interface ICourseMarketService {


    /**
     * 根据id查询课程营销信息
     * @param courseId
     * @return
     */
    CourseMarket findCourseMarketById(String courseId);


    /**
     * 编辑课程营销信息
     * @param
     * @param courseMarket
     * @return
     */
    ResponseResult update(CourseMarket courseMarket);
}
