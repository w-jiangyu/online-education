package com.lxw.manage.course.CourseController;

import com.lxw.api.course.CategoryControllerApi;
import com.lxw.framework.domain.course.ext.CategoryNode;
import com.lxw.manage.course.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CategoryController implements CategoryControllerApi {

    @Autowired
    CategoryService categoryService;

    @Override
    @GetMapping("category/list")
    public CategoryNode findList() {
        return categoryService.findList();
    }
}
