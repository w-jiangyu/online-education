package com.lxw.manage.course.dao;

import com.lxw.framework.domain.course.Category;
import com.lxw.framework.domain.course.ext.CategoryNode;
import com.lxw.framework.domain.course.ext.TeachplanNode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 课程分类mapper
 * Created by Administrator.
 */
@Component  //扫描
@Mapper
public interface CategoryMapper {
    CategoryNode findList();

}
