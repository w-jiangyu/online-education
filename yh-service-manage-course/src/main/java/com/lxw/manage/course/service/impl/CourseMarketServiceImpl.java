package com.lxw.manage.course.service.impl;


import com.lxw.framework.domain.course.CourseMarket;
import com.lxw.framework.model.response.CommonCode;
import com.lxw.framework.model.response.ResponseResult;
import com.lxw.manage.course.dao.CourseMarketRepository;
import com.lxw.manage.course.service.ICourseMarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CourseMarketServiceImpl implements ICourseMarketService {

    @Autowired
    CourseMarketRepository courseMarketRepository;//课程营销信息


    /**
     * 根据id查询课程营销信息
     * @param courseId
     * @return
     */
    @Override
    public CourseMarket findCourseMarketById(String courseId) {
        Optional<CourseMarket> byId = courseMarketRepository.findById(courseId);//主键就是课程id
        if (byId.isPresent()){
            return byId.get();
        }

        return null;
    }


    /**
     * 编辑课程营销信息
     * @param
     * @param courseMarket
     * @return
     */
    @Override
    public ResponseResult update(CourseMarket courseMarket) {
        courseMarketRepository.save(courseMarket);
        return new  ResponseResult(CommonCode.SUCCESS);
    }
}
