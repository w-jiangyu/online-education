package com.lxw.manage.course.service;


import com.lxw.framework.domain.course.ext.CategoryNode;

public interface CategoryService {

    CategoryNode findList();
}
