package com.lxw.manage.course.service;

import com.lxw.framework.domain.course.CourseBase;
import com.lxw.framework.domain.course.CoursePic;
import com.lxw.framework.domain.course.Teachplan;
import com.lxw.framework.domain.course.ext.TeachplanNode;
import com.lxw.framework.domain.course.response.CoursePublishResult;
import com.lxw.framework.domain.course.response.CourseView;
import com.lxw.framework.model.response.ResponseResult;
import org.springframework.data.domain.Page;

public interface ICourseService {


    /**
     * 根据courseId查询课程计划
     * @param courseId
     * @return
     */
    TeachplanNode findTeachplanList(String courseId);


    /**
     * 添加课程计划
     * @param teachplan
     * @return
     */
    ResponseResult addTeachplan(Teachplan teachplan);


    /**
     * 根据id查询课程计划
     * @param id
     * @return
     */
    Teachplan findTeachplanById(String id);

    /**
     * 根据id删除课程
     * @param id
     * @return
     */
    ResponseResult delTeachplan(String id);


    /**
     * 添加课程
     * @param courseBase
     * @return
     */
    ResponseResult addCourseBase(CourseBase courseBase);

    /**
     * 分页查询课程
     * @param page
     * @param pageSize
     */
    Page<CourseBase> findList(Integer page, Integer pageSize);


    /**
     * 根据id查询课程基本信息
     * @param courseId
     * @return
     */
    CourseBase findCourseBaseById(String courseId);


    /**
     * 编辑课程基本信息
     * @param courseId
     * @param courseBase
     * @return
     */
    CourseBase editCourseBase(String courseId, CourseBase courseBase);



    ResponseResult updateTeachaplan(Teachplan teachplan);


    /**
     * 保存课程图片
     * @param courseId
     * @param pic
     * @return
     */
    ResponseResult addCoursePic(String courseId, String pic);


    /**
     * 根据ID查询课程图片
     * @param courseId
     * @return
     */
    CoursePic findCoursePicById(String courseId);

    /**
     * 根据ID删除课程图片
     * @param courseId
     * @return
     */
    ResponseResult delCoursePicById(String courseId);

    CourseView getModelData(String courseId);

    CoursePublishResult preview(String id);

    /**
     * 课程发布
     * @param id
     * @return
     */
    CoursePublishResult publish(String id);
}
