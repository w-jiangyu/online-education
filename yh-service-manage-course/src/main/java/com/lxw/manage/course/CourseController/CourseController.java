package com.lxw.manage.course.CourseController;


import com.lxw.api.course.CourseControllerApi;
import com.lxw.client.CmsPageClient;
import com.lxw.framework.domain.course.CourseBase;
import com.lxw.framework.domain.course.CourseMarket;
import com.lxw.framework.domain.course.CoursePic;
import com.lxw.framework.domain.course.Teachplan;
import com.lxw.framework.domain.course.ext.TeachplanNode;
import com.lxw.framework.domain.course.response.CoursePublishResult;
import com.lxw.framework.domain.course.response.CourseView;
import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.CommonCode;
import com.lxw.framework.model.response.QueryResponseResult;
import com.lxw.framework.model.response.QueryResult;
import com.lxw.framework.model.response.ResponseResult;
import com.lxw.manage.course.service.ICourseMarketService;
import com.lxw.manage.course.service.ICourseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
public class CourseController implements CourseControllerApi {

    @Autowired
    ICourseService courseService;
    @Autowired
    ICourseMarketService courseMarketService;


    /**
     * @PathVariable 接受路径的参数
     * @param courseId
     * @return
     */
    @Override
    @GetMapping("teachplan/list/{courseId}")
    public TeachplanNode findTeachplanList(@PathVariable String courseId) {
        return courseService.findTeachplanList(courseId);
    }

    /**
     * 根据id查询课程计划
     * @param id
     * @return
     */
    @Override
    @GetMapping("teachplan/get/{id}")
    public Teachplan findTeachplanById(@PathVariable String id) {
        return courseService.findTeachplanById(id);
    }


    /**
     * 添加课程计划
     * @RequestBody  将前端传送的json数据封装为java对象
     * @RequestParam asfas?id=5
     * @param teachplan
     * @return
     */
    @Override
    @PostMapping ("teachplan/add")
    public ResponseResult addTeachplan(@RequestBody Teachplan teachplan) {
        return courseService.addTeachplan(teachplan);
    }


    /**
     * 删除课程计划
     * @param id
     * @return
     */
    @Override
    @DeleteMapping("teachplan/del/{id}")
    public ResponseResult delTeachplan(@PathVariable String id) {
        return courseService.delTeachplan(id);
    }

    @Override
    @PostMapping("teachplan/update")
    public ResponseResult updateTeachaplan(@RequestBody Teachplan teachplan) {
        return courseService.updateTeachaplan(teachplan);
    }

    /**
     * 添加课程
     * @param courseBase
     * @return
     */
    @Override
    @PostMapping("coursebase/add")
    public ResponseResult addCourseBase(@RequestBody CourseBase courseBase) {
        //非空校验
        if (StringUtils.isEmpty(courseBase.getName())){
            throw new  CustomException(CommonCode.FAIL);
        }

        return courseService.addCourseBase(courseBase);
    }

    /**
     * 分页查询课程
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    @GetMapping("coursebase/list/{page}/{pageSize}")
    public QueryResponseResult findList(@PathVariable Integer page,@PathVariable Integer pageSize) {
        Page<CourseBase> list = courseService.findList(page, pageSize);
        QueryResult<CourseBase> queryResult = new QueryResult<>();
        queryResult.setTotal(list.getTotalElements());
        queryResult.setList(list.getContent());
        return new QueryResponseResult(CommonCode.SUCCESS,queryResult);
    }


    /**
     * 根据id查询课程基本信息
     * @param courseId
     * @return
     */
    @Override
    @GetMapping("coursebase/get/{courseId}")
    public CourseBase findCourseBaseById(@PathVariable String courseId) {


        return courseService.findCourseBaseById(courseId);
    }


    /**
     * 编辑课程基本信息
     * @param courseId
     * @param courseBase
     * @return
     */
    @Override
    @PostMapping("coursebase/edit1/{courseId}")
    public ResponseResult editCourseBase(@PathVariable String courseId,@RequestBody CourseBase courseBase) {
        courseService.editCourseBase(courseId,courseBase);

        return new ResponseResult(CommonCode.SUCCESS);
    }


    /**
     * 根据id查询课程营销信息
     * @param courseId
     * @return
     */
    @Override
    @GetMapping("courseMaket/get/{courseId}")
    public CourseMarket findCourseMarketById(@PathVariable String courseId) {

        return courseMarketService.findCourseMarketById(courseId);
    }


    /**
     * 编辑课程营销信息
     * @param courseId
     * @param courseMarket
     * @return
     */
    @Override
    @PostMapping("courseMaket/update/{courseId}")
    public ResponseResult editCourseMarket(@PathVariable String courseId,@RequestBody CourseMarket courseMarket) {
        /**
         * 先查询课程信息是否存在
         */
        CourseBase courseBaseById = courseService.findCourseBaseById(courseId);
        if (courseBaseById==null){
            throw  new CustomException(CommonCode.FAIL);
        }

        courseMarket.setId(courseId);
        return courseMarketService.update(courseMarket);
    }

    /**
     * 添加课程图片
     * @param courseId
     * @param pic
     * @return
     */
    @Override
    @PostMapping("coursepic/add")
    public ResponseResult addCoursePic(String courseId, String pic) {
        //非空校验
        if (courseId==null||pic==null){
            throw new CustomException(CommonCode.FAIL);
        }
        return courseService.addCoursePic(courseId,pic);
    }


    /**
     * 根据ID查询课程图片
     * @param courseId
     * @return
     */

    @Override
    @GetMapping("coursepic/list/{courseId}")
    public CoursePic findCoursePicById(@PathVariable String courseId) {


        return courseService.findCoursePicById(courseId);
    }


    /**
     * 根据ID删除课程图片
     * @param courseId
     * @return
     */
    @Override
    @DeleteMapping("coursepic/delete")
    public ResponseResult delCoursePicById(@RequestParam String courseId) {
        return courseService.delCoursePicById(courseId);
    }


    @Autowired
    CmsPageClient cmsPageClient;

    @GetMapping("getpage")
    public Object getPage(String id) {
        return cmsPageClient.findById(id);
    }

    @Override
    @GetMapping("/getModelData/{courseId}")
    public CourseView getModelData(@PathVariable String courseId) {
        return courseService.getModelData(courseId);
    }

    @Override
    @GetMapping("/preview/{id}")
    public CoursePublishResult preview(@PathVariable String id) {


        return courseService.preview(id);
    }


    /**
     * 课程发布
     * @param id
     * @return
     */
    @Override
    @GetMapping("/publish/{id}")
    public CoursePublishResult postPage(@PathVariable String id) {

        return courseService.publish(id);
    }

}
