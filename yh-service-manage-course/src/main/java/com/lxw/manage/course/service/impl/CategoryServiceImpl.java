package com.lxw.manage.course.service.impl;

import com.lxw.framework.domain.course.ext.CategoryNode;
import com.lxw.manage.course.dao.CategoryMapper;
import com.lxw.manage.course.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public CategoryNode findList() {
        return categoryMapper.findList();
    }
}
