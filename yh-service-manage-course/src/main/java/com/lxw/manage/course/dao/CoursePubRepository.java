package com.lxw.manage.course.dao;

import com.lxw.framework.domain.course.CoursePub;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursePubRepository extends JpaRepository<CoursePub,String> {
}
