package com.lxw.manage_cms.dao;

import com.lxw.framework.domain.cms.CmsTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CmsTemplateDao extends MongoRepository<CmsTemplate,String> {
}
