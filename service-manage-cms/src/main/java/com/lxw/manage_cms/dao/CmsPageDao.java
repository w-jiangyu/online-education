package com.lxw.manage_cms.dao;

import com.lxw.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;
//                                                      String 主键的类型
public interface CmsPageDao extends MongoRepository<CmsPage,String> {


    /**
     * 根据页面名称和模版ID查询页面信息
     * @return
     */
    CmsPage findByPageNameAndTemplateId(String pageName,String templateId);

    /**
     *
     * @param pageName
     * @param siteId
     * @param pageWebPath
     * @return
     */
    CmsPage findByPageNameAndSiteIdAndPageWebPath(String pageName,String siteId,String pageWebPath);
}
