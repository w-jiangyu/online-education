package com.lxw.manage_cms.dao;


import com.lxw.framework.domain.system.SysDictionary;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DictionaryDao extends MongoRepository<SysDictionary,String> {
}
