package com.lxw.manage_cms.dao;

import com.lxw.framework.domain.cms.CmsConfig;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CmsConfigDao extends MongoRepository<CmsConfig,String> {

}
