package com.lxw.manage_cms.dao;

import com.lxw.framework.domain.cms.CmsSite;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CmsSiteDao extends MongoRepository<CmsSite,String> {
}
