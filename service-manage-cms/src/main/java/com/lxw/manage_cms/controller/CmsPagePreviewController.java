package com.lxw.manage_cms.controller;


import com.lxw.manage_cms.service.ICmsPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class CmsPagePreviewController {
    @Autowired
    ICmsPageService cmsPageService;


    @RequestMapping("preview/{pageId}")
    public void preview(@PathVariable String  pageId, HttpServletResponse response) throws IOException {
        String html=cmsPageService.preview(pageId);
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(html);
        response.getWriter().close();
        response.getWriter().flush();

    }

}
