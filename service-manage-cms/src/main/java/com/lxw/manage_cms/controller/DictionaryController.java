package com.lxw.manage_cms.controller;


import com.lxw.api.cms.DictionaryControllerApi;
import com.lxw.framework.domain.system.SysDictionary;
import com.lxw.manage_cms.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DictionaryController implements DictionaryControllerApi {
    @Autowired
    DictionaryService dictionaryService;


    @Override
    @GetMapping("/sys/dictionary/get/{dType}")
    public SysDictionary findByType(@PathVariable String dType) {
        return dictionaryService.findByType(dType);
    }
}
