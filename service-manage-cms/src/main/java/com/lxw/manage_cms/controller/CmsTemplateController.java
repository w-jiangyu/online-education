package com.lxw.manage_cms.controller;


import com.lxw.api.cms.CmsTemplateControllerApi;
import com.lxw.framework.domain.cms.CmsTemplate;
import com.lxw.framework.model.response.CommonCode;
import com.lxw.framework.model.response.QueryResponseResult;
import com.lxw.framework.model.response.QueryResult;
import com.lxw.framework.model.response.ResponseResult;
import com.lxw.manage_cms.service.ICmsTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("template")
public class CmsTemplateController implements CmsTemplateControllerApi {

    @Autowired
    ICmsTemplateService cmsTemplateService;


    @PostMapping("/list/findListTemplate/{siteId}/{templateName}")
    @Override
    public QueryResponseResult findListTemplate(String siteId, String templateName) {
        QueryResult<CmsTemplate> cmsTemplateQueryResult = new QueryResult<>();
        List<CmsTemplate> listTemplate = cmsTemplateService.findListTemplate(siteId, templateName);
        cmsTemplateQueryResult.setList(listTemplate);
        QueryResponseResult queryResponseResult = new QueryResponseResult(CommonCode.SUCCESS,cmsTemplateQueryResult);
        return queryResponseResult;
    }


    @GetMapping("/list/addTemplate")
    @Override
    public ResponseResult addTemplate(CmsTemplate cmsTemplate) {
        //非空校验

        if (cmsTemplate.getSiteId()==null || "".equals(cmsTemplate.getSiteId())){
            throw new RuntimeException("站点id为空");
        } if (cmsTemplate.getTemplateName()==null || "".equals(cmsTemplate.getTemplateName())){
            throw new RuntimeException("模板名称为空");
        } if (cmsTemplate.getTemplateFileId()==null || "".equals(cmsTemplate.getTemplateFileId())){
            throw new RuntimeException("模版文件Id为空");
        }

        cmsTemplateService.addTemplate(cmsTemplate);

        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(200);
        responseResult.setMessage("恭喜你，添加成功！！！");
        responseResult.setSuccess(true);
        return responseResult;
    }
}
