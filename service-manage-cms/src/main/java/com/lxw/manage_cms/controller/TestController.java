package com.lxw.manage_cms.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Controller
public class TestController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("test.html")
    public String test(Model model){

        Map<String,Object>map=new HashMap<>();
        map.put("name","zs");
        map.put("age",18);
        map.put("addr","河南省郑州市");

        model.addAttribute("map",map);
        return "test";
    }

    @GetMapping("banner.html")
    public String banner(Map<String,Object> map){
        //获取模型数据
        String forObject = restTemplate.getForObject(
                "http://localhost:31001/cms/page/getModelData/5a791725dd573c3574ee333f", String.class);
        //解析
        JSONObject jsonObject = JSON.parseObject(forObject);
        map.putAll(jsonObject);//${name}

        return "index_banner";
    }


    @GetMapping("course.html")
    public String course(Map<String,Object> map){
        // 获取模型数据
        String forObject = restTemplate.getForObject(
                "http://127.0.0.1:31200/course/getModelData/1", String.class);
        JSONObject jsonObject = JSON.parseObject(forObject);
        map.putAll(jsonObject);//${name}

        return "course";
    }
}
