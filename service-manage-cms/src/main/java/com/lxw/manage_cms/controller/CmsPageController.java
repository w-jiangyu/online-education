package com.lxw.manage_cms.controller;


import com.lxw.api.cms.CmsPageControllerApi;
import com.lxw.framework.domain.cms.CmsConfig;
import com.lxw.framework.domain.cms.CmsPage;
import com.lxw.framework.domain.cms.request.QueryPageRequest;
import com.lxw.framework.domain.cms.response.CmsCode;
import com.lxw.framework.domain.cms.response.CmsPageResult;
import com.lxw.framework.domain.course.response.CmsPostPageResult;
import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.CommonCode;
import com.lxw.framework.model.response.QueryResponseResult;
import com.lxw.framework.model.response.QueryResult;
import com.lxw.manage_cms.service.ICmsCofigService;
import com.lxw.manage_cms.service.ICmsPageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("page")
public class CmsPageController implements CmsPageControllerApi {
    @Autowired
    ICmsPageService cmsPageService;

    @Autowired
    ICmsCofigService cmsCofigService;

    @Override
    @GetMapping("list/{page}/{pageSize}")
    public QueryResponseResult findList(@PathVariable Integer page,@PathVariable Integer pageSize, QueryPageRequest qpr) {


        QueryResult<CmsPage> queryResult=new QueryResult();
//        List<CmsPage>list =new ArrayList<>();
//        CmsPage cmsPage=new CmsPage();
//        cmsPage.setPageName("测试成功");
//        list.add(cmsPage);

        Page<CmsPage> cmsPages=cmsPageService.findList(page,pageSize,qpr);

        queryResult.setList(cmsPages.getContent());//每页的数据
        queryResult.setTotal(cmsPages.getTotalElements());//总条数
        QueryResponseResult queryResponseResult=new QueryResponseResult(CommonCode.SUCCESS,queryResult);
        return queryResponseResult;
    }


    @PostMapping("add")
    @Override
    public CmsPageResult add(@RequestBody CmsPage cmsPage) {
        //非空校验
        if (StringUtils.isEmpty(cmsPage.getPageName())){
//            throw new RuntimeException("页面名称不能为空");
            throw new CustomException(CmsCode.CMS_PAGE_INAMEISNULL);
        }

        //业务校验和数据插入
        cmsPage.setPageId(null); //防止前端传入空字符串
        cmsPage=cmsPageService.save(cmsPage);


        CmsPageResult cmsPageResult=new CmsPageResult(CommonCode.SUCCESS,cmsPage);

        return cmsPageResult;
    }



    @Override
    @GetMapping("get/{pageId}")
    public CmsPage findById(@PathVariable  String pageId) {

        return cmsPageService.findById(pageId);
    }

    @Override
    @PutMapping("edit/{pageId}")
    public CmsPageResult edit(@PathVariable String pageId,@RequestBody CmsPage cmsPage) {
        //非空
        if (StringUtils.isEmpty(pageId)){
//            throw  new RuntimeException("pageId不能为空");
//            return new CmsPageResult(CommonCode.FAIL,null);
            throw new CustomException(CmsCode.CMS_PAGE_PAGMEISNULL);

        }


        cmsPageService.edit(pageId,cmsPage);

        CmsPageResult cmsPageResult=new CmsPageResult(CommonCode.SUCCESS,null);
        return cmsPageResult;
    }

    @Override
    @DeleteMapping("del/{pageId}")
    public CmsPageResult delById(@PathVariable String pageId) {

        CmsPageResult cmsPageResult=new CmsPageResult();
        cmsPageResult.setSuccess(true);

        //创建try/catch 快捷键 ctrl+alt+t
        try {
            cmsPageService.deleById(pageId);
            cmsPageResult.setCode(CommonCode.SUCCESS.code());
            cmsPageResult.setMessage("操作成功");
        } catch (Exception e) {

          e.printStackTrace();
          cmsPageResult.setCode(CommonCode.FAIL.code());
          cmsPageResult.setMessage("操作失败");
        }
        return cmsPageResult;
    }



    @Override
    @GetMapping("getModelData/{id}")
    public CmsConfig getModelData(@PathVariable String id) {


        return cmsCofigService.getModelData(id);
    }

    @Override
    @GetMapping("postPage/{pageId}")
    public CmsPageResult postPage(@PathVariable String pageId) {

        cmsPageService.postPage(pageId);
        return new CmsPageResult(CommonCode.SUCCESS,null);
    }


    /**
     * 一键发布页面
     * @param cmsPage
     * @return
     */
    @Override
    @PostMapping("postPageQuick")
    public CmsPostPageResult postPageQuick(@RequestBody CmsPage cmsPage) {
        return cmsPageService.postPageQuick(cmsPage);
    }


}
