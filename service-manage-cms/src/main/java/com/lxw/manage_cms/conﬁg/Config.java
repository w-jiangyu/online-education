package com.lxw.manage_cms.conﬁg;


import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Config {
    //定义交换机名称
    public static final String EXCHANGE_NAME="ex_routing_cms_postpage";


    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }


    //利用MongoDatabase读取数据库
    @Bean
    public GridFSBucket gridFSBucket(MongoDbFactory mongoDbFactory){
        MongoDatabase db = mongoDbFactory.getDb();

        return GridFSBuckets.create(db);
    }
}
