package com.lxw.manage_cms.service;

import com.lxw.framework.domain.cms.CmsTemplate;
import com.lxw.manage_cms.dao.CmsTemplateDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ICmsTemplateServiceImpl implements ICmsTemplateService{

    @Autowired
    CmsTemplateDao cmsTemplateDao;


    /**
     * 1. 模板查询
     * 2. 请求参数：（站点ID：精确匹配，模板名称：模糊匹配） 响应参数（QueryResponseResult）
     * @return
     */
    @Override
    public List<CmsTemplate> findListTemplate(String siteId, String templateName) {

        ExampleMatcher exampleMatcher = ExampleMatcher.matching();
        exampleMatcher = exampleMatcher.withMatcher("templateName", ExampleMatcher.GenericPropertyMatchers.contains());

        CmsTemplate cmsTemplate = new CmsTemplate();
        cmsTemplate.setSiteId(siteId);
        cmsTemplate.setTemplateName(templateName);

        Example<CmsTemplate> example = Example.of(cmsTemplate,exampleMatcher);
        List<CmsTemplate> all = cmsTemplateDao.findAll(example);
        return all;

    }


    /**
     * 1. 模板添加
     * 2. 请求参数：（CmsTemplate） 响应参数（ResponseResult）
     * 3. 模板名称、站点ID、模版文件Id：templateFileId不能为空
     * @param cmsTemplate
     */
    @Override
    public void addTemplate(CmsTemplate cmsTemplate) {
        //非空校验  模板名称、站点ID、模版文件Id：templateFileId不能为空
        if (StringUtils.isEmpty(cmsTemplate.getTemplateName())){
            throw new RuntimeException("模板Id不能为空");
        }
        if (StringUtils.isEmpty(cmsTemplate.getSiteId())){
            throw new RuntimeException("站点Id不能为空");
        }
        if (StringUtils.isEmpty(cmsTemplate.getTemplateFileId())){
            throw new RuntimeException("模板文件不能为空");
        }

        cmsTemplateDao.save(cmsTemplate);
    }



}
