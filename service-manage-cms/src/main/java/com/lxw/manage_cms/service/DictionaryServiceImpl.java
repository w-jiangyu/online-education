package com.lxw.manage_cms.service;


import com.lxw.framework.domain.system.SysDictionary;
import com.lxw.manage_cms.dao.DictionaryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DictionaryServiceImpl implements DictionaryService{

    @Autowired
    DictionaryDao dictionaryDao;


    @Override
    public SysDictionary findByType(String dType) {
        SysDictionary sysDictionary=new SysDictionary();
        sysDictionary.setDType(dType);//设置查询条件
        Example<SysDictionary> example=Example.of(sysDictionary);
        Optional<SysDictionary> one = dictionaryDao.findOne(example);//查询一条

        if (one.isPresent()){
            return one.get();
        }

        return null;
    }
}
