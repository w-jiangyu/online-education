package com.lxw.manage_cms.service;

import com.lxw.framework.domain.cms.CmsSite;

import java.util.List;

public interface ICmsSiteService {

    /**
     * 站点查询
     * @return
     */
    List<CmsSite> findListSite();
}
