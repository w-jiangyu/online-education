package com.lxw.manage_cms.service;

import com.lxw.framework.domain.cms.CmsConfig;
import com.lxw.manage_cms.dao.CmsConfigDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CmsCofigServiceImpl implements ICmsCofigService {

    @Autowired
    CmsConfigDao cmsConfigDao;

    @Override
    public CmsConfig getModelData(String id) {
        Optional<CmsConfig> byId = cmsConfigDao.findById(id);
        if (byId.isPresent()){
            return byId.get();
        }
        return null;
    }
}
