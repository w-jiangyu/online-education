package com.lxw.manage_cms.service;

import com.lxw.framework.domain.system.SysDictionary;

public interface DictionaryService {
    SysDictionary findByType(String dType);
}
