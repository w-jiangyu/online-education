package com.lxw.manage_cms.service;

import com.lxw.framework.domain.cms.CmsTemplate;

import java.util.List;

public interface ICmsTemplateService {

    /**
     * 1. 模板查询
     * 2. 请求参数：（站点ID：精确匹配，模板名称：模糊匹配） 响应参数（QueryResponseResult）
     * @return
     */
    List<CmsTemplate> findListTemplate(String siteId, String templateName);

    /**
     * 1. 模板添加
     * 2. 请求参数：（CmsTemplate） 响应参数（ResponseResult）
     * 3. 模板名称、站点ID、模版文件Id：templateFileId不能为空
     * @param cmsTemplate
     */
    void addTemplate(CmsTemplate cmsTemplate);
}
