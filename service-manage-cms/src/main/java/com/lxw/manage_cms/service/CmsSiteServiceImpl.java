package com.lxw.manage_cms.service;


import com.lxw.framework.domain.cms.CmsSite;
import com.lxw.manage_cms.dao.CmsSiteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CmsSiteServiceImpl implements ICmsSiteService{


    @Autowired
    CmsSiteDao cmsSiteDao;


    /**
     * 站点查询
     * @return
     */
    @Override
    public List<CmsSite> findListSite() {
        List<CmsSite> all = cmsSiteDao.findAll();
        return all;
    }
}
