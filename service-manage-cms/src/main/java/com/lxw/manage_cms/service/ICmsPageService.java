package com.lxw.manage_cms.service;

import com.lxw.framework.domain.cms.CmsPage;
import com.lxw.framework.domain.cms.request.QueryPageRequest;
import com.lxw.framework.domain.course.response.CmsPostPageResult;
import org.springframework.data.domain.Page;

public interface ICmsPageService {

    Page<CmsPage> findList(Integer page, Integer pageSize, QueryPageRequest qpr);

    CmsPage save(CmsPage cmsPage);

    /**
     * 根据pageid 查询页面信息
     * @param pageId
     * @return
     */
    CmsPage findById(String pageId);


    /**
     * 页面编辑
     * @param pageId
     * @param cmsPage
     */
    void edit(String pageId, CmsPage cmsPage);


    /**
     * 根据Id删除页面信息
     * @param pageId
     */
    void deleById(String pageId);

    /**
     * 页面预览
     * @param pageId
     * @return
     */
    String preview(String pageId);


    /**
     * 页面发布
     * @param pageId
     */
    void postPage(String pageId);


    /**
     * 一键发布页面
     * @param cmsPage
     * @return
     */
    CmsPostPageResult postPageQuick(CmsPage cmsPage);
}
