package com.lxw.manage_cms.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lxw.framework.domain.cms.CmsPage;
import com.lxw.framework.domain.cms.CmsSite;
import com.lxw.framework.domain.cms.CmsTemplate;
import com.lxw.framework.domain.cms.request.QueryPageRequest;
import com.lxw.framework.domain.cms.response.CmsCode;
import com.lxw.framework.domain.course.response.CmsPostPageResult;
import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.CommonCode;
import com.lxw.manage_cms.conﬁg.Config;
import com.lxw.manage_cms.dao.CmsPageDao;
import com.lxw.manage_cms.dao.CmsSiteDao;
import com.lxw.manage_cms.dao.CmsTemplateDao;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class CmsPageServiceImpl implements ICmsPageService{

    @Autowired
    CmsPageDao cmsPageDao;

    @Autowired
    RestTemplate restTemplate; //模型数据

    @Autowired
    CmsTemplateDao cmsTemplateDao;

    @Autowired
    GridFsTemplate gridFsTemplate;

    @Autowired
    GridFSBucket gridFSBucket;

    @Autowired
    RabbitTemplate rabbitTemplate;




    @Override
    public Page<CmsPage> findList(Integer page, Integer pageSize, QueryPageRequest qpr) {

        CmsPage cmsPage=new CmsPage();

        //非空验证
        if (StringUtils.isNotEmpty(qpr.getTemplateId())){
            cmsPage.setTemplateId(qpr.getTemplateId());
        }
        if (StringUtils.isNotEmpty(qpr.getSiteId())){
            cmsPage.setSiteId(qpr.getSiteId());

        }
        if (StringUtils.isNotEmpty(qpr.getPageAliase())){
            cmsPage.setPageAliase(qpr.getPageAliase());

        }

        //页面别名模糊查询
        ExampleMatcher.GenericPropertyMatcher contains = ExampleMatcher.GenericPropertyMatchers.contains();
        //第一个参数 模糊查询参数，第二个参数为 模糊查询条件
        ExampleMatcher exampleMatcher=ExampleMatcher.matching().withMatcher("pageAliase",contains);
        Example<CmsPage> example=Example.of (cmsPage,exampleMatcher);


        //页码从0开始
        page = 0;
        PageRequest of = PageRequest.of(page,pageSize);
        Page<CmsPage> all1 = cmsPageDao.findAll(example,of);
        return all1;
    }


    //页面添加
    @Override
    public CmsPage save(CmsPage cmsPage) {
        //条件：创建页面名称、站点Id、页面webpath为唯一索引
        long count = getPageCount(cmsPage);
        if (count>0){
            throw  new CustomException(CmsCode.CMS_PAGE_ISMUSTONE);
        }

        //保存
        return cmsPageDao.save(cmsPage);
    }

    //提取条件 ： ctri+alt+m
    private long getPageCount(CmsPage cmsPage) {
        CmsPage cmsPage2=new CmsPage();
        cmsPage2.setPageName(cmsPage.getPageName());
        cmsPage2.setSiteId(cmsPage.getSiteId());
        cmsPage2.setPageWebPath(cmsPage.getPageWebPath());
        Example<CmsPage> example=Example.of(cmsPage2);
        return cmsPageDao.count(example);
    }


    //根据id查询页面信息
    @Override
    public CmsPage findById(String pageId) {

        Optional<CmsPage> byId = cmsPageDao.findById(pageId);
        if (byId.isPresent()){
            return byId.get();
        }

        return null;
    }


    /**
     * 页面编辑
     * @param pageId
     * @param cmsPage
     */
    @Override
    public void edit(String pageId, CmsPage cmsPage) {

        //1.查询
        Optional<CmsPage> byId = cmsPageDao.findById(pageId);
        if (!byId.isPresent()){
//            throw new RuntimeException("");
            throw  new CustomException(CmsCode.CMS_PAGE_INAMEISNULL);

        }

        CmsPage page=byId.get();
        String pageName=page.getPageName();

        //2.更新 （注意：判断非空和空字符串）
        if (StringUtils.isNotEmpty(cmsPage.getSiteId())) {
            page.setSiteId(cmsPage.getSiteId());
        }

        if (StringUtils.isNotEmpty(cmsPage.getTemplateId())) {
            page.setTemplateId(cmsPage.getTemplateId());
        }

        if (StringUtils.isNotEmpty(cmsPage.getPageName())) {
            page.setPageName(cmsPage.getPageName());
        }

        if (StringUtils.isNotEmpty(cmsPage.getPageAliase())) {
            page.setPageAliase(cmsPage.getPageAliase());
        }

        if (StringUtils.isNotEmpty(cmsPage.getPageWebPath())) {
            page.setPageWebPath(cmsPage.getPageWebPath());
        }

        if (StringUtils.isNotEmpty(cmsPage.getPagePhysicalPath())) {
            page.setPagePhysicalPath(cmsPage.getPagePhysicalPath());
        }

        if (StringUtils.isNotEmpty(cmsPage.getPageType())) {
            page.setPageType(cmsPage.getPageType());
        }




        if (StringUtils.isNotEmpty(cmsPage.getDataUrl())) {
            page.setDataUrl(cmsPage.getDataUrl());
        }

        //3.创建页面名称、站点Id、页面webpath为唯一索引
        if (StringUtils.isNotEmpty(cmsPage.getPageName())&&!cmsPage.getPageName().equals(pageName)){
            long count = getPageCount(page);
            if (count>0){
//                throw  new RuntimeException("创建页面名称、站点Id、页面webpath为唯一");
                throw  new CustomException(CmsCode.CMS_PAGE_ISMUSTONE);
            }
        }
        cmsPageDao.save(page);//save 更新
    }


    @Override
    public void deleById(String pageId) {
        cmsPageDao.deleteById(pageId);
    }


    /**
     * 页面预览
     * @param pageId
     * @return
     */
    @Override
    public String preview(String pageId) {
        //1.根据id获取页面信息
        CmsPage cmsPage = findById(pageId);
        if(cmsPage==null){
            log.error("pageId:{},不存在",pageId);
            throw new CustomException(CommonCode.FAIL);
        }

        //2.获取dataUrl并且远程获取模型数据
        String dataUrl = cmsPage.getDataUrl();
        if (StringUtils.isEmpty(dataUrl)){
            throw  new CustomException(CommonCode.FAIL);
        }



            //2.1远程获取模型数据
        String forObject = restTemplate.getForObject(dataUrl,String.class);
        JSONObject jsonObject = JSON.parseObject(forObject);

        //3.以模板id获取模版
        String templateId = cmsPage.getTemplateId();
        Optional<CmsTemplate> byId = cmsTemplateDao.findById(templateId);
        if (!byId.isPresent()){
            throw  new CustomException(CommonCode.FAIL);
        }
        CmsTemplate cmsTemplate = byId.get();
        String templateFileId = cmsTemplate.getTemplateFileId();//模板文件id
        //读取文件
        String tempStr=getTempStr(templateFileId);


        //4.执行静态化
        Template template= getTemplate(tempStr);
        String htmlStr=null;
        try {
            htmlStr = FreeMarkerTemplateUtils.processTemplateIntoString(template, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return htmlStr;
    }



    /**
     * 读取gridFs文件
     * @param templateFileId
     * @return
     */
    private String getTempStr(String templateFileId) {
        try {
            GridFSFile id = gridFsTemplate.findOne(
                    Query.query(Criteria.where("_id").is(templateFileId)));

            //2.获取文件夹
            GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(id.getObjectId());

            //3.获取文件合并流
            GridFsResource gridFsResource = new GridFsResource(id,gridFSDownloadStream);
            String tempString = IOUtils.toString(gridFsResource.getInputStream(), "UTF-8");
            return tempString;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    /**
     * 获取模板
     * @param tempStr
     * @param
     * @return
     */
    private Template getTemplate(String tempStr) {
        try {
            Configuration configuration=new Configuration(Configuration.getVersion());
            StringTemplateLoader templateLoader = new StringTemplateLoader();
            templateLoader.putTemplate("test",tempStr);
            configuration.setTemplateLoader(templateLoader);

            configuration.setDefaultEncoding("UTF-8");//设计编码
            Template  template = configuration.getTemplate("test");
            return template;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    /**
     * 页面发布
     * @param pageId
     */
    @Override
    public void postPage(String pageId) {
        //1.页面静态化  使用preview上的方法
        CmsPage cmsPage=findById(pageId);
        String previewHtml = this.preview(pageId);

           //保存静态化页面到girdFs
        try {
            InputStream inputStream = IOUtils.toInputStream(previewHtml, "UTF-8");
            String fileName = cmsPage.getPageName();
            //保存文件到girdFs
            ObjectId store = gridFsTemplate.store(inputStream, fileName);
            //静态化页面ID
            cmsPage.setHtmlFileId(String.valueOf(store));
            //把内容更新
            cmsPageDao.save(cmsPage);
        } catch (IOException e) {
            e.printStackTrace();
            throw  new CustomException(CommonCode.FAIL);
        }

        //2.写入磁盘 MQ（消息队列）
        //格式：{"pageId":"11111"}
        Map<String,Object> map=new HashMap<>();
        map.put("pageId",pageId);

        //站点id=key
        String routingkey=cmsPage.getSiteId();
        rabbitTemplate.convertAndSend(Config.EXCHANGE_NAME,routingkey,JSON.toJSONString(map));


    }





    /**
     * 一键发布页面
     *
     * @param cmsPage
     * @return
     */

    @Autowired
    CmsSiteDao cmsSiteDao;

    @Override
    public CmsPostPageResult postPageQuick(CmsPage cmsPage) {
        // 1.保存页面信息
        CmsPage cmsPage1 = cmsPageDao.findByPageNameAndSiteIdAndPageWebPath(cmsPage.getPageName(), cmsPage.getSiteId()
                , cmsPage.getPageWebPath());
        if(cmsPage1==null){
            // 插入数据
            this.save(cmsPage);
        }else {
            cmsPage.setPageId(cmsPage1.getPageId());
            cmsPageDao.save(cmsPage);
        }
        // 2. 页面发布
        this.postPage(cmsPage.getPageId());

        //3.页面 Url = cmsSite.siteDomain + cmsSite.siteWebPath + cmsPage.pageWebPath + cmsPage.pageName
        String pageUrl = null;
        Optional<CmsSite> byId = cmsSiteDao.findById(cmsPage.getSiteId());//查询站点信息
        if(!byId.isPresent()){
            throw new CustomException(CommonCode.FAIL);
        }

        CmsSite cmsSite = byId.get();
        pageUrl = cmsSite.getSiteDomain()+cmsSite.getSiteWebPath()
                +cmsPage.getPageWebPath()+cmsPage.getPageName();


        return new CmsPostPageResult(CommonCode.SUCCESS,pageUrl);
    }


}
