package com.lxw.manage_cms;


import com.alibaba.fastjson.JSON;
import com.lxw.framework.domain.cms.CmsPage;
import com.lxw.manage_cms.dao.CmsPageDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class MongoDbCrudTest {
    @Autowired
    CmsPageDao cmsPageDao;


    @Test
    public void testAdd(){
        //测试数据添加
        CmsPage cmsPage=new CmsPage();
        cmsPage.setPageName("测试页面添加");
        CmsPage insert=cmsPageDao.insert(cmsPage);
        log.info("添加返回数据{}",insert);
    }


    @Test
    public void testAdd2(){
        //测试数据添加
        CmsPage cmsPage=new CmsPage();
        cmsPage.setPageName("测试页面添加2");
        CmsPage insert=cmsPageDao.save(cmsPage);
        log.info("添加返回数据2{}",insert);
    }


    @Test
    public void testUpdate(){
        //测试数据更新
        CmsPage cmsPage=new CmsPage();
        cmsPage.setPageId("612626aab8bde14a98d09926");
        cmsPage.setPageName("测试页面更新");
        CmsPage insert=cmsPageDao.save(cmsPage);//保存或者更新,根据id是否存在 判断更新或者保存
        log.info("添加返回更新{}",insert);
    }


    @Test
    public void testFind(){
        //测试数据查询全部
        List<CmsPage> all=cmsPageDao.findAll();

        log.info("添加返回数据{}，类型{}",all,all.size());

        //测试根据id查询
        Optional<CmsPage> byId = cmsPageDao.findById("612626aab8bde14a98d09926");
        //Optional 它是判断对象是否为空
        if (byId.isPresent()){
            CmsPage cmsPage = byId.get();
            log.info("查询数据为:{}",cmsPage);
        }else {
            log.info("当前查询为空");
        }

        //分页查询   org.springframework.data.domain.PageRequest;
        int pagrNo=0;//页码从0开始
        PageRequest of = PageRequest.of(0,2);
        Page<CmsPage> all1 = cmsPageDao.findAll(of);
        //getContent() 页面的内容  getTotalPages()总页码
        log.info("查询{}",all1.getContent(),all1.getTotalPages());





        //条件查询 Example.of
        CmsPage cmsPage=new CmsPage();
        cmsPage.setPageName("测试页面更新");
        Example<CmsPage> example=Example.of(cmsPage);
        List<CmsPage> all2 = cmsPageDao.findAll(example);
        log.info("获取数据为{}",all2);


        //分页条件查询
//        CmsPage cmsPage1=new CmsPage();
//        cmsPage1.setPageId("5a754adf6abb500ad05688d9");
//        Example.of(cmsPage);
//



        //自定义查询

    }


    //删除  作业：如何判断是否删除
    @Test
    public void testDel(){
        cmsPageDao.deleteById("612626beb8bde1376cc202d1");
    }

    @Test
    public void testDel0(){
        cmsPageDao.deleteById("612626beb8bde1376cc202d1");

    }


    //模糊条件分页查询
    @Test
    public void testDe2(){
        //条件查询
        CmsPage cmsPage=new CmsPage();
//        cmsPage.setPageId("5a751fab6abb5044e0d19ea1"); //精度条件查询相当于==


        //模糊查询
        cmsPage.setPageAliase("课程");//查询条件
        //模糊查询条件 like  %%
        ExampleMatcher.GenericPropertyMatcher contains = ExampleMatcher.GenericPropertyMatchers.contains();
        //第一个参数 模糊查询参数，第二个参数为 模糊查询条件
        ExampleMatcher exampleMatcher=ExampleMatcher.matching().withMatcher("pageAliase",contains);
        Example<CmsPage> example=Example.of (cmsPage,exampleMatcher);

        List<CmsPage> all2 = cmsPageDao.findAll(example);
        log.info("获取数据为{}",all2);

        //分页查询
        int pagrNo=0;//页码从0开始
        PageRequest of = PageRequest.of(0,2);
        Page<CmsPage> all = cmsPageDao.findAll(example, of);
        log.info("获取数据为{}", JSON.toJSONString(all));


    }
}
