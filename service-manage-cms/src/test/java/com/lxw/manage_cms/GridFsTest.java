package com.lxw.manage_cms;


import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

/**
 * 读取文件
 */

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class GridFsTest {
    @Autowired
    GridFsTemplate gridFsTemplate;

    @Autowired
    GridFSBucket gridFSBucket;


    //根据内容上传
    @Test
    public void upload(){
        //存储文件名称
        String cnt="<link rel=\"stylesheet\" href=\"http://fhs.com/plugins/normalize-css/normalize.css\" />\n" +
                "<link rel=\"stylesheet\" href=\"http://fhs.com/plugins/bootstrap/dist/css/bootstrap.css\" />\n" +
                "<link rel=\"stylesheet\" href=\"http://fhs.com/css/page-learing-index.css\" />\n" +
                "\n" +
                "<div class=\"banner-roll\">\n" +
                "    <div class=\"banner-item\">\n" +
                "        <#list model as obj>\n" +
                "            <div class=\"item\" style=\"background-image: url(${obj.value});\"></div>\n" +
                "        </#list>\n" +
                "    </div>\n" +
                "    <div class=\"indicators\"></div>\n" +
                "</div>\n" +
                "<#--${name} 测试-->\n" +
                "\n" +
                "<script type=\"text/javascript\" src=\"http://fhs.com/plugins/jquery/dist/jquery.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://fhs.com/plugins/bootstrap/dist/js/bootstrap.js\"></script>\n" +
                "<script type=\"text/javascript\">\n" +
                "    var tg = $('.banner-item .item');\n" +
                "    var num = 0;\n" +
                "    for (i = 0; i < tg.length; i++) {\n" +
                "        $('.indicators').append('<span></span>');\n" +
                "        $('.indicators').find('span').eq(num).addClass('active');\n" +
                "    }\n" +
                "\n" +
                "    function roll() {\n" +
                "        tg.eq(num).animate({\n" +
                "            'opacity': '1',\n" +
                "            'z-index': num\n" +
                "        }, 1000).siblings().animate({\n" +
                "            'opacity': '0',\n" +
                "            'z-index': 0\n" +
                "        }, 1000);\n" +
                "        $('.indicators').find('span').eq(num).addClass('active').siblings().removeClass('active');\n" +
                "        if (num >= tg.length - 1) {\n" +
                "            num = 0;\n" +
                "        } else {\n" +
                "            num++;\n" +
                "        }\n" +
                "    }\n" +
                "    $('.indicators').find('span').click(function() {\n" +
                "        num = $(this).index();\n" +
                "        roll();\n" +
                "    });\n" +
                "    var timer = setInterval(roll, 3000);\n" +
                "    $('.banner-item').mouseover(function() {\n" +
                "        clearInterval(timer)\n" +
                "    });\n" +
                "    $('.banner-item').mouseout(function() {\n" +
                "        timer = setInterval(roll, 3000)\n" +
                "    });\n" +
                "</script>";

        try {
            InputStream inputStream = IOUtils.toInputStream(cnt, "UTF-8");
            ObjectId store = gridFsTemplate.store(inputStream, "banner.ftl");
            log.info("store:{}",store);//612f91daccaca12a7461f00d
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

  //根据地址上传
    @Test
    public void upload2(){
        try {

            FileInputStream inputStream = new FileInputStream(new File("E:\\edu\\service-manage-cms\\src\\main\\resources\\templates\\course.ftl"));
            ObjectId store = gridFsTemplate.store(inputStream, "course2.ftl");
            log.info("store:{}",store);//612f935bccaca11988c44a2e
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //删除
    @Test
    public void dele(){

        gridFsTemplate.delete(Query.query(Criteria.where("_id").is("612f935bccaca11988c44a2e")));
    }


    //读取文件
    @Test
    public void redif(){
        try {
            //1.获取集合  根据id查询
            GridFSFile id = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is("612f91daccaca12a7461f00d")));
            log.info("[{}]",id);
            //2.获取文件夹
            GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(id.getObjectId());

            //3.获取文件合并流
            GridFsResource gridFsResource = new GridFsResource(id,gridFSDownloadStream);

            //4.输出文件
            InputStream inputStream = gridFsResource.getInputStream();

            IOUtils.copy(inputStream,new FileOutputStream("D:/ZFB/"+id.getFilename()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
