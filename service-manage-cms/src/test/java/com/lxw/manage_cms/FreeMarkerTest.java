package com.lxw.manage_cms;


import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * 执行静态化
 */

@SpringBootTest
@RunWith(SpringRunner.class)
public class FreeMarkerTest {


    @Test
    public void test1(){
        //创建配置类加载器
        Configuration configuration=new Configuration(Configuration.getVersion());

        //设置模板文件夹
        try {
            String path = FreeMarkerTest.class.getResource("/").getPath();
            //配置模板绝对路径
            File file=new File(path+"/../classes/templates");
            configuration.setDirectoryForTemplateLoading(file);
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //通过名称来获取模板
            Template template = configuration.getTemplate("test.ftl");
            //数据模型
            Map<String,Object> mapData=new HashMap<>();
            Map<String,Object> map=new HashMap<>();
            map.put("name","王先生");
            map.put("age",22);
            map.put("addr","河南省郑州市");
            mapData.put("map",map);
            //静态化
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mapData);


            System.out.println(html);
            //输出文件
            InputStream inputStream = IOUtils.toInputStream(html, "UTF-8");
            IOUtils.copy(inputStream,new FileOutputStream("D:/ZFB/test.html"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
