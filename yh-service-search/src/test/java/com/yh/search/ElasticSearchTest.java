package com.yh.search;

import com.alibaba.fastjson.JSON;
import com.lxw.framework.domain.course.CoursePub;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class ElasticSearchTest {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test
    public void search() throws IOException {
        SearchRequest searchRequest = new SearchRequest("yh_course");
        searchRequest.types("doc");

        /////////
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        String keyword = "数据";
//        boolQueryBuilder.filter(QueryBuilders.termQuery("grade","200001"));

        searchSourceBuilder.query(boolQueryBuilder);
            ///////////////////

            // limit 0,2  分页
            int page = 1,size=2;

        searchSourceBuilder.from((page-1)*size);
        searchSourceBuilder.size(size);

            // 高亮处理
            HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.preTags("<font style=\"color:red\">");
        highlightBuilder.postTags("</font>");
        highlightBuilder.field("name");
        searchSourceBuilder.highlighter(highlightBuilder);

        searchRequest.source(searchSourceBuilder);
            SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        log.info("{}", JSON.toJSONString(search));
            SearchHits searchHits = search.getHits();
            SearchHit[] hits = searchHits.getHits();
            List<CoursePub> coursePubList = new ArrayList<>();
        for(SearchHit hit:hits){

//            System.out.println(hit.getId());
//
//            Map<String, Object> map = hit.getSourceAsMap();
//            System.out.println(map.get("name"));
                String sourceAsString = hit.getSourceAsString();
                CoursePub coursePub = JSON.parseObject(sourceAsString, CoursePub.class);

                // 获取高亮的filed
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                HighlightField name = highlightFields.get("name");
                if(name!=null){
                    Text[] fragments = name.getFragments();
                    StringBuilder sb = new StringBuilder();
                    for (Text text:fragments) {
                        sb.append(text.string());
                    }
                    coursePub.setName(sb.toString());
                }

                coursePubList.add(coursePub);
//            System.out.println(coursePub);
            }
        System.out.println(coursePubList);
        }
    }