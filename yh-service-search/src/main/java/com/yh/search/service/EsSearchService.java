package com.yh.search.service;

import com.lxw.framework.domain.search.CourseSearchParam;
import com.lxw.framework.model.response.QueryResponseResult;

public interface EsSearchService {
    /**
     * 课程带分页条件查询（带全文检索）
     * @param page
     * @param size
     * @param courseSearchParam
     * @return
     */
    QueryResponseResult list(int page, int size, CourseSearchParam courseSearchParam);
}
