package com.yh.search.controller;

import com.lxw.api.search.EsCourseControllerApi;
import com.lxw.framework.domain.search.CourseSearchParam;
import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.QueryResponseResult;
import com.yh.search.service.EsSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search/course")
public class EsCourseController implements EsCourseControllerApi {
    @Autowired
    EsSearchService esSearchService;

    @Override
    @GetMapping(value="/list/{page}/{size}")
    public QueryResponseResult list(@PathVariable int page, @PathVariable  int size, CourseSearchParam courseSearchParam) throws CustomException {

        return esSearchService.list(page,size,courseSearchParam);
    }
}