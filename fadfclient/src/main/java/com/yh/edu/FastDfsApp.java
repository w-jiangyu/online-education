package com.yh.edu;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastDfsApp {
    public static void main(String[] args) {
        SpringApplication.run(FastDfsApp.class,args);
    }
}
