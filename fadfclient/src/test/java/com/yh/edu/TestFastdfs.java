package com.yh.edu;


import org.apache.commons.io.IOUtils;
import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileOutputStream;
import java.io.IOException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestFastdfs {

    @Value("${fastdfs.tracker_servers}")
    String tracker_servers;
    @Value("${fastdfs.connect_timeout_in_seconds}")
    int connect_timeout_in_seconds;
    @Value("${fastdfs.network_timeout_in_seconds}")
    int network_timeout_in_seconds;
    @Value("${fastdfs.charset}")
    String charset;


    //加载fdfs的配置
    private void initFdfsConfig() {
        try {
            ClientGlobal.initByTrackers(tracker_servers);
            ClientGlobal.setG_connect_timeout(connect_timeout_in_seconds);
            ClientGlobal.setG_network_timeout(network_timeout_in_seconds);
            ClientGlobal.setG_charset(charset);
        } catch (Exception e) {
            e.printStackTrace();
            //初始化文件系统出错
        }
    }

    /**
     * 上传
     */
    @Test
    public void  upload() throws Exception {

        initFdfsConfig();

        // 1.tracker （踹可扫完）
        TrackerClient trackerClient = new TrackerClient();
        //获取trackerServer
        TrackerServer trackerServer = trackerClient.getConnection();
        System.out.println(trackerServer.getInetSocketAddress());

        // 2.storageServer (死到危机)
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);
        System.out.println(storageServer.getInetSocketAddress());

        //3.上传
//        StorageClient1 storageClient1 = new StorageClient1(trackerServer,storageServer);
//        String fileName = "D:\\yh\\nnn.jpg";
//        String jpg = storageClient1.upload_file1(fileName, "jpg", null);
//        System.out.println(jpg);

    }

    @Test
    public void download() throws Exception {
        initFdfsConfig();

        // 1.tracker
        TrackerClient trackerClient = new TrackerClient();
        //获取trackerServer
        TrackerServer trackerServer = trackerClient.getConnection();
        System.out.println(trackerServer.getInetSocketAddress());

        // 2.storageServer
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);
        System.out.println(storageServer.getInetSocketAddress());

        //3.上传

        StorageClient1 storageClient1 = new StorageClient1(trackerServer,storageServer);
        byte[] bytes = storageClient1.download_file1("/group1/M00/00/00/rBEAC2E9hwaAbWG7AASwbTHfWyk752.jpg");
        IOUtils.write(bytes,new FileOutputStream("D:/yh/edu/nnn.jpg"));

        int i = storageClient1.download_file1("/group1/M00/00/00/rBEAC2E9hwaAbWG7AASwbTHfWyk752.jpg", "D:/yh/edu/nnn.jpg");
        System.out.println(i+"------");
        if (i==0){
            System.out.println("下载成功");
        }
    }

    @Test
    public void down(){
        initFdfsConfig();
        try {
            //创建客户端
            TrackerClient tracker = new TrackerClient();
            TrackerServer trackerServer = tracker.getConnection();
            StorageServer storageServer = null;
            StorageClient storageClient = new StorageClient(trackerServer, storageServer);
            byte[] fileInfo = storageClient.download_file("group1", "/M00/00/00/rBEAC2E9hwaAbWG7AASwbTHfWyk752.jpg");
            IOUtils.write(fileInfo,new FileOutputStream("D:\\1.jpg"));
        } catch (IOException | MyException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void query() throws Exception {
        initFdfsConfig();

        // 1.tracker
        TrackerClient trackerClient = new TrackerClient();
        //获取trackerServer
        TrackerServer trackerServer = trackerClient.getConnection();
        System.out.println(trackerServer.getInetSocketAddress());

        // 2.storageServer
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);
        System.out.println(storageServer.getInetSocketAddress());

        //3.上传

        StorageClient1 storageClient1 = new StorageClient1(trackerServer, storageServer);
        FileInfo fileInfo = storageClient1.query_file_info1("group1" + "/M00/00/00/rBEAC2E9hwaAbWG7AASwbTHfWyk752.jpg");
        System.out.println(fileInfo);
    }
}
