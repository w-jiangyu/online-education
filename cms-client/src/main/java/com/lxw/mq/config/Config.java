package com.lxw.mq.config;


import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;

@Configuration
public class Config {
    //交换机的名称
    public   String exchangeName="ex_routing_cms_postpage";

    @Value("${fhs.mq.queue}")
    private String queueName;

    @Value("${fhs.mq.routingKey}")
    private String routingKey;




    /**
     * 路由模式
     * 除了要绑定交换机外还要绑定路由key
     */
    //声明发布路由模式队列
    @Bean
    public Queue postPageMq() {
        return new Queue(queueName);//队列一
    }

    //声明发布路由模式的交换机
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(exchangeName);
    }
    //将队列绑定到交换机
    @Bean
    public Binding dirBinding(DirectExchange directExchange, Queue postPageMq) {
        return BindingBuilder.bind(postPageMq).to(directExchange).with(routingKey);
    }

    //利用MongoDatabase读取数据库
    @Bean
    public GridFSBucket gridFSBucket(MongoDbFactory mongoDbFactory){
        MongoDatabase db = mongoDbFactory.getDb();

        return GridFSBuckets.create(db);
    }
}
