package com.lxw.mq.dao;

import com.lxw.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

//                                                      String 主键的类型
public interface CmsPageDao extends MongoRepository<CmsPage,String> {
}
