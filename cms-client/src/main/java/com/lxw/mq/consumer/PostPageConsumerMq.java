package com.lxw.mq.consumer;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lxw.mq.service.ICmsPageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 消费者
 */
@Component
@Slf4j
public class PostPageConsumerMq {


    @Autowired
    ICmsPageService cmsPageService;

    @RabbitListener(queues = "${fhs.mq.queue}")
    @RabbitHandler
    public void process(String msg){
        log.info("页面消费{}",msg);

        //解析json字符串
        JSONObject jsonObject = JSON.parseObject(msg);
        String pageId=jsonObject.getString("pageId");
        //把业务保存的静态化html文件写入到磁盘  1.读取文件  2.磁盘的物理路径 3.写入
        try {
            cmsPageService.writeFile(pageId);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("页面发布失败，接受数据为：{}",msg);
        }

    }
}
