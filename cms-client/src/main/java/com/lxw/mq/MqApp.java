package com.lxw.mq;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.lxw"})//扫描所有com.lxw目录下的
@EntityScan("com.lxw.framework.domain.cms")//扫描实体类
public class MqApp {
    public static void main(String[] args) {
        SpringApplication.run(MqApp.class,args);
    }
}
