package com.lxw.framework.exception;

import com.lxw.framework.model.response.ResultCode;
import lombok.Data;


@Data  //get,set方法
public class CustomException extends RuntimeException {
    private ResultCode resultCode;

    public CustomException(ResultCode resultCode) {
        this.resultCode = resultCode;
    }


}
