package com.lxw.framework.model.response;

import lombok.Data;
import lombok.ToString;

/**
 * 响应
 */
@Data
@ToString
public class QueryResponseResult extends ResponseResult {

    QueryResult queryResult;

    //成功 或者 失败
    public QueryResponseResult(ResultCode resultCode,QueryResult queryResult){
        super(resultCode);
       this.queryResult = queryResult;
    }

}
