package com.lxw.framework.interceptor;


import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.CommonCode;
import com.lxw.framework.model.response.ResponseResult;
import com.lxw.framework.model.response.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice  //帮助我们捕获异常
@Slf4j
public class ExceptionCatch {


    /**
     * 自定义异常
     * @param e
     */
    @ExceptionHandler(CustomException.class)//捕获
    @ResponseBody
    public ResponseResult CustomException(CustomException e){
        ResultCode resultCode=e.getResultCode();
        e.printStackTrace();//异常打印到控制台
        return new ResponseResult(resultCode);

    }


    /**
     * 捕获未知异常
     * @param e
     */
    @ExceptionHandler(Exception.class)//捕获
    public ResponseResult unKnownException(Exception e){
        e.printStackTrace();//异常打印到控制台
        return new ResponseResult(CommonCode.SERVER_ERROR);
    }
}
