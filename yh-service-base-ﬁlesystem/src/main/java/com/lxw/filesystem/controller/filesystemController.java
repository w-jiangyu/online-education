package com.lxw.filesystem.controller;

import com.lxw.api.filesystem.filesystemControllerApi;
import com.lxw.filesystem.service.IFileService;
import com.lxw.framework.domain.filesystem.FileSystem;
import com.lxw.framework.domain.filesystem.response.UploadFileResult;
import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.CommonCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class filesystemController implements filesystemControllerApi {

    @Autowired
    IFileService fileService;

    /**
     * 文件上传
     * @param multipartFile
     * @param filetag
     * @param businesskey
     * @param metadata
     * @return
     */
    @Override
    @PostMapping("/filesystem/upload")
    public UploadFileResult upload(@RequestParam("file") MultipartFile multipartFile, String filetag, String businesskey, String metadata) {
        if (multipartFile==null){
            throw new CustomException(CommonCode.FAIL);
        }
        FileSystem fileId= fileService.upload(multipartFile,filetag,businesskey,metadata);

        return new UploadFileResult(CommonCode.SUCCESS,fileId);
    }
}
