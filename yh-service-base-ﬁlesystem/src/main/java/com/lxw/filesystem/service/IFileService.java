package com.lxw.filesystem.service;

import com.lxw.framework.domain.filesystem.FileSystem;
import org.springframework.web.multipart.MultipartFile;

public interface IFileService {

    FileSystem upload(MultipartFile multipartFile, String filetag, String businesskey, String metadata);
}
