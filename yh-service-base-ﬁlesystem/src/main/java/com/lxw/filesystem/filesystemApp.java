package com.lxw.filesystem;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.lxw"})
 public class filesystemApp {
    public static void main(String[] args) {
        SpringApplication.run(filesystemApp.class,args);
    }
}
