package com.lxw.filesystem.dao;

import com.lxw.framework.domain.filesystem.FileSystem;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FileSystemDao extends MongoRepository<FileSystem,String> {
}
