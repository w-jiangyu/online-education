package com.lxw.mq.consumer;


import com.lxw.mq.MqConst;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


/**
 * 消费者
 */
@Component //扫描
public class MqConsumer {

    @RabbitListener(queues = MqConst.TEST_QUEUEQ)//监听
    @RabbitHandler //接收并处理信息
    public void process(String msg){
        System.out.println(msg);
    }


    @RabbitListener(queues = MqConst.FANOUT1)//监听
    @RabbitHandler //接收并处理信息
    public void process2(String msg){
        System.out.println(msg);
    }



    @RabbitListener(queues = MqConst.FANOUT3)//监听
    @RabbitHandler //接收并处理信息
    public void process3(String msg){
        System.out.println(msg);
    }



    @RabbitListener(queues = MqConst.DIRECT_ERR)//监听
    @RabbitHandler //接收并处理信息
    public void process4(String msg){
        System.out.println(msg);
    }


    @RabbitListener(queues = MqConst.TOP_ERROR)//监听
    @RabbitHandler //接收并处理信息
    public void process5(String msg){
        System.out.println(msg);
    }


//    @RabbitListener(queues = "ex_routing_cms_postpage")//监听
//    @RabbitHandler //接收并处理信息
//    public void process6(String msg){
//        System.out.println(msg);
//    }



}
