package com.lxw.mq;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class ProducerTest {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    public void testQueue(){
        // 队列模式 生产者发送消息
        /**
         * convertAndSend  数据的转换
         * MqConst.TEST_QUEUEQ   消息队列名
         * LocalDateTime.now()   时间戳
         */
        rabbitTemplate.convertAndSend(MqConst.TEST_QUEUEQ,"hello"+ LocalDateTime.now());
    }


    @Test
    public void testQueue2(){
        // 队列模式 生产者发送消息
        /**
         * convertAndSend  数据的转换
         * MqConst.TEST_QUEUEQ   消息队列名
         * LocalDateTime.now()   时间戳
         */
        rabbitTemplate.convertAndSend(MqConst.FANOUT1,"hello"+ LocalDateTime.now());
    }


    @Test
    public void testQueue3(){
        // 订阅模式 生产者发送消息
        /**
         * convertAndSend  数据的转换
         * 1.MqConst.TEST_QUEUEQ   交换机名
         * 2.路由key
         * 3.内容
         * LocalDateTime.now()   时间戳
         */
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_FANOUT,"","hello,王先生"+ LocalDateTime.now());
    }


    @Test
    public void testQueue4(){
        // 路由模式 生产者发送消息
        /**
         * convertAndSend  数据的转换
         * 第一个mq 是声明交换机名称
         * MqConst.TEST_QUEUEQ   消息队列名
         * LocalDateTime.now()   时间戳
         */
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_DIRECT,MqConst.DIRECT_ERR,"今天天气很差"+ LocalDateTime.now());
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_DIRECT,MqConst.DIRECT_INFO,"今天天气很好"+ LocalDateTime.now());
    }



    @Test
    public void testQueue5(){
        // 主题模式 生产者发送消息
        /**
         * convertAndSend  数据的转换
         * 第一个mq是声明交换机名称
         * MqConst.TEST_QUEUEQ   消息队列名
         * LocalDateTime.now()   时间戳
         */
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_TOPIC,MqConst.TOP_ERROR,"今天天气很差"+ LocalDateTime.now());
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_TOPIC,MqConst.TOP_INFO,"今天天气很好"+ LocalDateTime.now());
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_TOPIC,MqConst.TOP_WARN,"今天天气还好"+ LocalDateTime.now());
    }


    @Test
    public void testQueue6(){
        // 订阅模式 生产者发送消息
        /**
         * convertAndSend  数据的转换
         * 1.  交换机名
         * 2.路由key
         * 3.内容
         * LocalDateTime.now()   时间戳
         */
        rabbitTemplate.convertAndSend("ex_routing_cms_postpage","5a751fab6abb5044e0d19ea1","hello"+ LocalDateTime.now());
    }
}
