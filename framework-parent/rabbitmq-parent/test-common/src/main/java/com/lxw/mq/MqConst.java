package com.lxw.mq;

public class MqConst {
    public final static String TEST_QUEUEQ = "TEST_QUEUEQ";

    public final static String EXCHANGE_FANOUT = "exchange.fanout"; // 订阅交换机
    public final static String EXCHANGE_DIRECT = "exchange.direct"; // ROUTING(路由模式)交换机
    public final static String EXCHANGE_TOPIC = "exchange.topic"; // TOPIC（主题模式）交换机
    public final static String EXCHANGE_HEADER = "exchange.header"; // HEADER交换机

    public final static String FANOUT1 = "FANOUT1";
    public final static String FANOUT2 = "FANOUT2";
    public final static String FANOUT3 = "FANOUT3";


    public final static String DIRECT_ERR = "DIRECT_ERR";
    public final static String DIRECT_INFO = "DIRECT_INFO";
    public final static String DIRECT_WARN = "DIRECT_WARN";


    public final static String TOP_PR = "MSG.*";
    public final static String TOP_PR2 = "MSG.#";

    public final static String TOP_ERROR = "TOP_ERROR";
    public final static String TOP_INFO = "TOP_INFO";
    public final static String TOP_WARN = "TOP_WARN";
}
