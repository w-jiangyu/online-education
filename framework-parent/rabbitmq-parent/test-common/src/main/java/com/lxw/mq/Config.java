package com.lxw.mq;


import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class Config {
    private Map<String,Object>args=new HashMap<>();

    /**
     * 添加消息队列
     *
     * Queue:    import org.springframework.amqp.core.Queue;
     * @return
     */
    @Bean
    public Queue testQueue(){
        return new Queue(MqConst.TEST_QUEUEQ,true,false,false,args);
    }






    /**声明发布订阅模式的交换机
     * Exchange 交换机 模式不同 交换机也不同
     *
     * @return
     */
    @Bean
    public FanoutExchange fanout() {
        return new FanoutExchange(MqConst.EXCHANGE_FANOUT);
    }

    //声明发布订阅模式队列
    @Bean
    public Queue fanoutQueue1() {
        return new Queue(MqConst.FANOUT1);//队列一
    }
    @Bean
    public Queue fanoutQueue2() {
        return new Queue(MqConst.FANOUT2);//队列二
    }

    @Bean
    public Queue fanoutQueue3() {
        return new Queue(MqConst.FANOUT3);//队列二
    }



    /**
     * 将队列一绑定到交换机
     * BindingBuilder.bind(fanoutQueue1).to(fanout)  将。。。绑定。。
     * @param fanout
     * @param fanoutQueue1
     * @return
     */
    @Bean
    public Binding fanoutBinding1(FanoutExchange fanout, Queue fanoutQueue1) {
        return BindingBuilder.bind(fanoutQueue1).to(fanout);
    }

    //将队列二绑定到交换机
    @Bean
    public Binding fanoutBinding2(FanoutExchange fanout, Queue fanoutQueue2) {
        return BindingBuilder.bind(fanoutQueue2).to(fanout);
    }

    //将队列三绑定到交换机
    @Bean
    public Binding fanoutBinding3(FanoutExchange fanout, Queue fanoutQueue3) {
        return BindingBuilder.bind(fanoutQueue3).to(fanout);
    }


    /**
     * 路由模式
     * 除了要绑定交换机外还要绑定路由key
     */
    //声明发布路由模式队列
    @Bean
    public Queue dirQueue1() {
        return new Queue(MqConst.DIRECT_ERR);//队列一
    }

    //声明发布路由模式的交换机
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(MqConst.EXCHANGE_DIRECT);
    }
    //将队列绑定到交换机
    @Bean
    public Binding dirBinding2(DirectExchange directExchange, Queue dirQueue1) {
        return BindingBuilder.bind(dirQueue1).to(directExchange).with("DIRECT_ERR");
    }
    @Bean
    public Binding dirBinding3(DirectExchange directExchange, Queue dirQueue1) {
        return BindingBuilder.bind(dirQueue1).to(directExchange).with("DIRECT_INFO");
    }


    /**
     * 主题模式
     */
    @Bean
    public Queue topQueue1() {
        return new Queue(MqConst.TOP_ERROR);//队列一
    }
    @Bean
    public Queue topQueue2() {
        return new Queue(MqConst.TOP_INFO);//队列二
    }
    @Bean
    public Queue topQueue3() {
        return new Queue(MqConst.TOP_WARN);//队列三
    }

    //声明发布主题模式的交换机
    @Bean
    public TopicExchange topicExchange () {
        return new TopicExchange(MqConst.EXCHANGE_TOPIC);
    }

    //将队列绑定到交换机
    @Bean
    public Binding dirBinding4(TopicExchange topicExchange, Queue topQueue1) {
        return BindingBuilder.bind(topQueue1).to(topicExchange).with(MqConst.TOP_ERROR);
    }

    //将队列绑定到交换机(绑定通配符)
    @Bean
    public Binding dirBinding5(TopicExchange topicExchange, Queue topQueue1) {
        return BindingBuilder.bind(topQueue1).to(topicExchange).with(MqConst.TOP_PR);
    }
}
