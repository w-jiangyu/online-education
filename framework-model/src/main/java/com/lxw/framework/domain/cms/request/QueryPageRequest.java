package com.lxw.framework.domain.cms.request;

import com.lxw.framework.model.request.RequestData;
import lombok.Data;

/**
 * 继承RequestData  目的是为了扩展 请求
 */
@Data
public class QueryPageRequest extends RequestData {
    //站点id
    private String siteId;
    //页面ID
    private String pageId;
    //页面名称
    private String pageName;
    //别名
    private String pageAliase;
    //模版id
    private String templateId;
}
