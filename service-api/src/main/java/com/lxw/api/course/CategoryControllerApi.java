package com.lxw.api.course;

import com.lxw.framework.domain.course.ext.CategoryNode;
import io.swagger.annotations.Api;

@Api(tags = {"分类相关操作"})
public interface CategoryControllerApi {
    public CategoryNode findList();
}
