package com.lxw.api.course;


import com.lxw.framework.domain.course.CourseBase;
import com.lxw.framework.domain.course.CourseMarket;
import com.lxw.framework.domain.course.CoursePic;
import com.lxw.framework.domain.course.Teachplan;
import com.lxw.framework.domain.course.ext.TeachplanNode;
import com.lxw.framework.domain.course.response.CoursePublishResult;
import com.lxw.framework.domain.course.response.CourseView;
import com.lxw.framework.model.response.QueryResponseResult;
import com.lxw.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "课程相关")
public interface CourseControllerApi {


    @ApiOperation(value = "根据courseId查询课程计划")
    public TeachplanNode findTeachplanList(String courseId);

    @ApiOperation(value = "根据id查询课程计划")
    public Teachplan findTeachplanById(String id);


    @ApiOperation(value = "添加课程计划")
    public ResponseResult addTeachplan(Teachplan teachplan);


    @ApiOperation(value = "删除课程计划")
    public ResponseResult delTeachplan(String id);

    @ApiOperation(value = "修改课程计划")
    public ResponseResult updateTeachaplan(Teachplan teachplan);


    @ApiOperation(value = "添加课程")
    public ResponseResult addCourseBase(CourseBase courseBase);



    @ApiOperation(value = "分页查询课程")
    public QueryResponseResult findList(Integer page,Integer pageSize);

    @ApiOperation(value = "根据id查询课程基本信息")
    public CourseBase findCourseBaseById(String courseId);

    @ApiOperation(value = "修改课程基本信息")
    ResponseResult editCourseBase(String courseId, CourseBase courseBase);


    @ApiOperation(value = "根据id查询课程营销信息")
    public CourseMarket findCourseMarketById(String courseId);

    @ApiOperation(value = "编辑课程营销信息")
    ResponseResult editCourseMarket(String courseId, CourseMarket courseMarket);

    @ApiOperation(value = "添加课程图片")
    public ResponseResult addCoursePic(String courseId,String pic);

    @ApiOperation(value = "根据ID删除课程图片")
    public ResponseResult delCoursePicById(String courseId);

    @ApiOperation(value = "根据ID查询课程图片")
    public CoursePic findCoursePicById(String courseId);


    @ApiOperation(value = "根据ID获取课程模型数据")
    CourseView getModelData(String courseId);

    @ApiOperation("预览课程")
    public CoursePublishResult preview(String id);

    @ApiOperation("课程发布")
    public CoursePublishResult postPage(String id);

}
