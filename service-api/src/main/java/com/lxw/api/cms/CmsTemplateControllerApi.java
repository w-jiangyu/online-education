package com.lxw.api.cms;

import com.lxw.framework.domain.cms.CmsTemplate;
import com.lxw.framework.model.response.QueryResponseResult;
import com.lxw.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "模板信息",description = "模板信息的CRUD操作")
public interface CmsTemplateControllerApi {

    @ApiOperation(value = "模板信息查询：站点Id：精确匹配，模板名称：模糊查询")
    QueryResponseResult findListTemplate(String siteId,String templateName);


    @ApiOperation(value = "模板添加：模板名称，站点id，模板文件id，templateFileId不为空")
    ResponseResult addTemplate(CmsTemplate cmsTemplate);
}
