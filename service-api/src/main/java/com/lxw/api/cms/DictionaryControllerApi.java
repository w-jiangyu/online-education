package com.lxw.api.cms;

import com.lxw.framework.domain.system.SysDictionary;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
public interface DictionaryControllerApi {

    @ApiOperation(value = "根据类型查询数据字典")
    SysDictionary findByType(String dType);
}
