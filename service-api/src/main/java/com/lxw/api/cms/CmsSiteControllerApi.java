package com.lxw.api.cms;


import com.lxw.framework.model.response.QueryResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "站点信息",description = "站点信息中的CRUD操作")
public interface CmsSiteControllerApi {

    @ApiOperation(value = "查询所有站点")
    QueryResponseResult findListSite();
}
