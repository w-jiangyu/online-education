package com.lxw.api.cms;

import com.lxw.framework.domain.cms.CmsConfig;
import com.lxw.framework.domain.cms.CmsPage;
import com.lxw.framework.domain.cms.request.QueryPageRequest;
import com.lxw.framework.domain.cms.response.CmsPageResult;
import com.lxw.framework.domain.course.response.CmsPostPageResult;
import com.lxw.framework.model.response.QueryResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 各个项目定义的接口
 * 目的：1.集中管理  2.方便微服务（finl）调用
 */

@Api(value = "页面管理",description = "页面管理的CRUD")
public interface CmsPageControllerApi {

    //查询cms页面信息   QueryResponseResult响应数据
    @ApiOperation(value = "分页条件查询页面信息")
    public QueryResponseResult findList(Integer page, Integer pageSize, QueryPageRequest qpr);


    /**条件
     * 1. 页面添加
     * 2. 请求参数（CmsPage） 响应参数（CmsPageResult）
     * 请求参数{“name”:"","age"：“”}
     * 响应包装类： 统一的ResponseResult：{code,msg,data}
     * 3. 创建页面名称、站点Id、页面webpath为唯一索引
     */
    @ApiOperation(value = "页面添加")
    CmsPageResult add(CmsPage cmsPage);



    @ApiOperation(value = "通过ID查询页面")
    CmsPage findById(String pageId);


    @ApiOperation(value = "页面信息编辑")
    CmsPageResult edit(String pageId,CmsPage cmsPage);


    @ApiOperation(value = "根据ID删除页面信息")
    CmsPageResult   delById(String pageId);


    @ApiOperation(value = "获取模型数据")
    CmsConfig getModelData(String id);



    @ApiOperation(value = "页面发布")
    CmsPageResult postPage(String pageId);


    @ApiOperation("一键发布页面")
    CmsPostPageResult postPageQuick(CmsPage cmsPage);

}


