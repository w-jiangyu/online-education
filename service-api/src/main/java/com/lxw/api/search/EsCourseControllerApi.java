package com.lxw.api.search;

import com.lxw.framework.domain.search.CourseSearchParam;
import com.lxw.framework.exception.CustomException;
import com.lxw.framework.model.response.QueryResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = {"课程搜索"})
public interface EsCourseControllerApi {
    @ApiOperation("课程搜索")
    public QueryResponseResult list(int page, int size, CourseSearchParam courseSearchParam) throws CustomException;
}